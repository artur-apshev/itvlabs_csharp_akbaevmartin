﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Forms;

namespace WeatherApp
{
    public partial class Form1 : Form
    {
        const string APIKEY = "4abb78f08b41726fedb5f5df5e8d475b";

        string textBoxCityPlaceholderText = "Enter city name...";
        public Form1()
        {
            InitializeComponent();
            labelHumidity.ResetText();
            labelTemperature.ResetText();
            labelPressure.ResetText();
            buttonGet.Focus();
        }

        private async void buttonGet_Click(object sender, EventArgs e)
        {
            string cityName = textBoxCity.Text;
            if (cityName != textBoxCityPlaceholderText)
            {
                string requestUrl = $"http://api.openweathermap.org/data/2.5/weather?q={cityName}&APPID={APIKEY}&units=metric";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync(requestUrl);
                var responseBody = await response.Content.ReadAsStringAsync();

                var json = JsonConvert.DeserializeObject<WeatherData>(responseBody);
                if (json.cod == "200")
                {
                    labelHumidity.Text = $"Humidity: {json.main.humidity}%";
                    labelTemperature.Text = $"Temperature: {json.main.temp}";
                    labelPressure.Text = $"Pressure: {json.main.pressure}";
                }
                else
                {
                    MessageBox.Show($"City with name '{cityName}' not found.");
                }

            }
            else
                MessageBox.Show("Enter city name.");
            textBoxCity.Focus();
        }

        private void Form1_Load1(object sender, EventArgs e)
        {
            textBoxCity.Text = textBoxCityPlaceholderText;
            buttonGet.Focus();
        }

        private void TextBoxCity_Leave(object sender, EventArgs e)
        {
            if (textBoxCity.Text == "")
            {
                textBoxCity.Text = textBoxCityPlaceholderText;
                textBoxCity.ForeColor = Color.Gray;
            }
        }

        private void TextBoxCity_Enter(object sender, EventArgs e)
        {
            if (textBoxCity.Text == textBoxCityPlaceholderText)
            {
                textBoxCity.Text = "";
                textBoxCity.ForeColor = Color.Black;
            }
        }
    }
}
