﻿using System;
using System.Collections.Generic;

namespace Lab3
{
    public class DataGenerator
    {
        public enum Order
        {
            Random,
            ByGrowUp,
            ByGrowDoun
        }
        public static List<double> GenerateData(int count, Order order=Order.Random)
        {
            List<double> result = new List<double>();
            Random rand = new Random();
            switch (order)
            {
                case Order.Random:
                    for (int i = 0; i < count; ++i)
                    {
                        result.Add(rand.NextDouble() + rand.Next());
                    }
                    break;

                case Order.ByGrowUp:
                    for (int i = 0; i < count; ++i)
                    {
                        result.Add(i);
                    }
                    break;

                case Order.ByGrowDoun:
                    for (int i = count - 1; i >= 0; --i)
                    {
                        result.Add(i);
                    }
                    break;
                default:
                    throw new ArgumentException($" Unknown order type { order }");
            }
            return result;
        }
    }
}
