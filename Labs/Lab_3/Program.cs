﻿using System;
using System.Collections.Generic;

// • Имеется массив чисел double, например 100 элементов.
//   Сделать сортировку по возрастанию пузырьком и слиянием, c выводом количества операций сравнения каждым алгоритмом.
// • Поэкспериментировать с количеством операций в зависимости от распределения элементов в массиве:
// o Первый случай когда массив строго упорядочен;
// o второй случай когда массив обратно упорядочен, 
// o третий случай подбор чисел генератором случайных чисел.
// Оба алгоритма должны обязательно проходить одни и те же unit test-ы.Сделать как минимум 5 тестовых наборов, каждый не менее 100 чисел.

//•	Имеются предыдущие алгоритмы сортировки.
// Сгенерировать случайным образом наборы чисел из 1000, 10000, 100000, 1000000 элементов. 
// Сделать сортировку каждым из алгоритмов со сгенерированными массивами чисел.  
// Посчитать количество выполненных операций сравнения с каждым алгоритмом и каждым набором данных. 
// Сделать оценку сложности работы каждого алгоритма. Наборы массивов в этом задании выводить не нужно.

namespace Lab3
{
    class Program
    {
        const int MIN_ARRAY_COUNT = 10;
        const int MAX_ARRAY_COUNT = 100000;
        const int STEP_MULTIPLYER = 10;
        const int MAX_ARRAY_COUNT_FOR_PRINT = 10;

        static void Main(string[] args)
        {
            for (int count = MIN_ARRAY_COUNT; count <= MAX_ARRAY_COUNT; count *= STEP_MULTIPLYER)
            {
                CheckSorters(new MergeSorter(), count);
                CheckSorters(new BubbleSorter(), count);
            }
            Console.WriteLine("Program finished.");
            Console.ReadKey();
        }
        
        static void CheckSorters(ISorter Sorter, int listCount)
        {
            foreach (var sortBy in Enum.GetValues(typeof(DataGenerator.Order)))
            {
                List<double> unsorted = DataGenerator.GenerateData(listCount, order: (DataGenerator.Order)sortBy);
                if (listCount <= MAX_ARRAY_COUNT_FOR_PRINT)
                {
                    PrintArray(unsorted);
                }

                List<double> sorted = Sorter.Sort(unsorted);

                if (listCount <= MAX_ARRAY_COUNT_FOR_PRINT)
                {
                    PrintArray(sorted);
                }

                Console.WriteLine($"Shift number for {Sorter} with N == {listCount} and {sortBy} array: {Sorter.compareNumber}\n");
            }

            Console.WriteLine();
        }

        static void PrintArray(List<double> array)
        {
            array.ForEach(item => Console.Write("{0, 5} ", item));
            Console.WriteLine();
        }
    }
}
