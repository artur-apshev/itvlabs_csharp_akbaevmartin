﻿﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab3
{
    public class MergeSorter : ISorter
    {
        public long compareNumber { get; private set; }

        delegate double MyDeleg(double val);
        MyDeleg defaultKey = (double x) => { return x; };

        private List<double> doSort(List<double> unsorted, Delegate key)
        {
            if (unsorted.Count <= 1)
                return unsorted;
            int middle = unsorted.Count / 2;

            List<double> left = unsorted.Take(middle).ToList();
            List<double> right = unsorted.Skip(middle).ToList();

            left = doSort(left, key);
            right = doSort(right, key);
            return Merge(left, right, key);
        }

        public List<double> Sort(List<double> unsorted, Delegate key = null, bool reverse = false)
         {
            key = key ?? defaultKey;
            
            compareNumber = 0;
            List<double> result = doSort(unsorted, key);

            if (reverse)
            {
                result.Reverse();
            }
            return result;
        }

        private List<double> Merge(List<double> left, List<double> right, Delegate key)
        {
            List<double> result = new List<double>();

            while (left.Count > 0 || right.Count > 0)
            {
                compareNumber++;

                if (left.Count > 0 && right.Count > 0)
                {
                    if ((double)key.DynamicInvoke(left.First()) <= (double)key.DynamicInvoke(right.First()))  //Comparing First two elements to see which is smaller
                    {
                        result.Add(left.First());
                        left.Remove(left.First());      //Rest of the list minus the first element
                    }
                    else
                    {
                        result.Add(right.First());
                        right.Remove(right.First());
                    }
                }
                else if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                }
                else if (right.Count > 0)
                {
                    result.Add(right.First());
                    right.Remove(right.First());
                }
            }
            return result;
        }
    }
}
