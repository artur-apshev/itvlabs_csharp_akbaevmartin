using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lab1Test
{
    using Lab2_2;

    [TestClass]
    public class UnitTestLab2_2
    {
        [TestMethod]
        public void TestCheckEquation()
        {
            const double A = 1.0;
            const double B = 2.0;
            const double C = 3.0;
            const double M = -2.0;
            const double N = 2.0;

            const double D = 1.0;
            Assert.IsTrue(Equation.Check(A, B, C, M, N, D));
        }
    }

}