using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lab1Test
{
    using Lab2_1;
    using System;

    [TestClass]
    public class UnitTestLab2_1
    {
        [TestMethod]
        public void TestCostOfTravel()
        {
            double distance = 67;
            double consumption = 8.5;
            double rubPerLiter = 23.7;
            const double ETALON = 134.9715;
            double result = TripCostCalc.TripCost(
                distance:distance,
                fuelConsumption:consumption,
                petrolPrice:rubPerLiter
                );

            Assert.IsTrue(Math.Abs(ETALON - result) < Double.Epsilon, $"Incorrect result: {ETALON} != {result}");
        }
    }

}