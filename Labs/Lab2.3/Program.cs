﻿using System;

// Составьте программу, которая преобразует введенное с клавиатуры дробное число в денежный формат.
// Например, число 12,348 должно быть преобразовано к виду 12 руб. 35 коп.Ниже представлен рекомендуемый вид диалога во время работы программы.
// Преобразование числа в денежный формат.
// Введите дробное число – 23,6
// 23.6 руб. – это 23 руб. 60 коп.


namespace Lab2_3
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Write("Enter value: ");
                if (double.TryParse(Console.ReadLine(), out double input))
                {
                    Money money = new Money(input);
                    Console.WriteLine($"{input} is {money}.\n");
                }
                else
                {
                    Console.WriteLine("Incorrect input!");
                }

                Console.WriteLine("Press any key for continue, press Escape for exit.");

            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }

}
